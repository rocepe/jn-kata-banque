package tests.src.main.java;

import org.junit.Assert;
import org.junit.Test;
import src.main.java.Account;
import src.main.java.Operation;

import java.util.ArrayList;
import java.util.List;

public class TestAccountHistory {

    @Test
    public void testHistoryEmptyAccount() {
        Account account = new Account();
        Assert.assertEquals(0, account.history().size());
    }

    @Test
    public void testHistoryAccount_Size() {
        Account account = new Account();
        account.deposit(150);
        account.withdrawal(50);
        account.withdrawal(30);
        account.withdrawal(70);
        Assert.assertEquals(4, account.history().size());
    }

    @Test
    public void testHistoryAccount_Content() {
        Account account = new Account();
        account.deposit(150);
        account.withdrawal(50);
        account.withdrawal(30);
        account.withdrawal(70);

        List<Operation> operations = account.history();
        // check Deposit
        Assert.assertEquals("Deposit", operations.get(0).toString().split(",")[1].split(" ")[1]);
        Assert.assertEquals("150.0", operations.get(0).toString().split(",")[1].split(" ")[4]);
        Assert.assertEquals("150.0", operations.get(0).toString().split(",")[2].split(" ")[6]);

        // check Withdrawal 1
        Assert.assertEquals("Withdrawal", operations.get(1).toString().split(",")[1].split(" ")[1]);
        Assert.assertEquals("50.0", operations.get(1).toString().split(",")[1].split(" ")[4]);
        Assert.assertEquals("100.0", operations.get(1).toString().split(",")[2].split(" ")[6]);

        // check Withdrawal 2
        Assert.assertEquals("Withdrawal", operations.get(2).toString().split(",")[1].split(" ")[1]);
        Assert.assertEquals("30.0", operations.get(2).toString().split(",")[1].split(" ")[4]);
        Assert.assertEquals("70.0", operations.get(2).toString().split(",")[2].split(" ")[6]);

        // check Withdrawal 1
        Assert.assertEquals("Withdrawal", operations.get(3).toString().split(",")[1].split(" ")[1]);
        Assert.assertEquals("70.0", operations.get(3).toString().split(",")[1].split(" ")[4]);
        Assert.assertEquals("0.0", operations.get(3).toString().split(",")[2].split(" ")[6]);
    }
}
