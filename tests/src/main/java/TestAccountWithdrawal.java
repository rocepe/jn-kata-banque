package tests.src.main.java;
import org.junit.*;
import src.main.java.Account;

public class TestAccountWithdrawal {

    @Test
    public void testWithdrawalEmptyAccount() {
        Account account = new Account();
        Assert.assertEquals(0, account.withdrawal(100), 0);
    }

    @Test
    public void testWithdrawalAccount_Possible() {
        Account account = new Account();
        account.deposit(150);
        Assert.assertEquals(50,
                account.withdrawal(50), 0);
        Assert.assertEquals(30,
                account.withdrawal(30), 0);
        Assert.assertEquals(70,
                account.withdrawal(70), 0);
    }

    @Test
    public void testWithdrawalAccount_TooBigValue() {
        Account account = new Account();
        account.deposit(150);
        Assert.assertEquals(0,
                account.withdrawal(1000), 0);
    }
}
