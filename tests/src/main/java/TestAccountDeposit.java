package tests.src.main.java;
import org.junit.*;
import src.main.java.Account;

public class TestAccountDeposit {

    @Test
    public void testDepositEmptyAccount() {
        Account account = new Account();
        Assert.assertEquals(100, account.deposit(100), 0);
    }

    @Test
    public void testDepositEmptyAccount_TooBigValue() {
        Account account = new Account();
        Assert.assertEquals(0,
                account.deposit(account.getMAX_AMOUNT() + 1), 0);
    }
}
