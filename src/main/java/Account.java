package src.main.java;

import java.util.ArrayList;
import java.util.List;

public class Account {
    private final double MAX_AMOUNT = 1000000000; // too big numbers can generate errors

    private double account_amount = 0;
    private final ArrayList<Operation> account_operations;

    public Account() {
        super();
        account_operations = new ArrayList<>();
    }

    public double getMAX_AMOUNT() {
        return MAX_AMOUNT;
    }

    public double deposit(double aDeposit) {
        if (aDeposit > MAX_AMOUNT || account_amount > MAX_AMOUNT)
            return 0;
        account_amount += aDeposit;
        account_operations.add(new Operation(Operation.OperationType.Deposit, aDeposit, account_amount));
        return account_amount;
    }

    public double withdrawal(double aWithdrawal) {
        if (account_amount == 0 || account_amount < aWithdrawal)
            return 0;
        account_amount -= aWithdrawal;
        account_operations.add(new Operation(Operation.OperationType.Withdrawal, aWithdrawal, account_amount));
        return aWithdrawal;
    }

    public List<Operation> history() {
        return account_operations;
    }
}
