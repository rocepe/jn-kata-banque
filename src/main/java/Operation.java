package src.main.java;

import java.util.Date;

public class Operation {

    public enum OperationType {
        Deposit, Withdrawal
    }

    private final OperationType operationType;
    private final Date date;
    private final double amount;
    private final double balance; // account balance after operation

    protected Operation(OperationType aOperationType, double aAmount, double aBalance) {
        operationType = aOperationType;
        date = new Date();
        amount = aAmount;
        balance = aBalance;
    }

    public String toString() {
        String opType = (operationType.equals(OperationType.Deposit)) ? "Deposit" : "Withdrawal";
        return "Date: " + date + ", " + opType + " for amount " + amount +
                ", new balance after operation is " + balance + " .";
    }
}
